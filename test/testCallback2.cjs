// Import the listInfo function from the callback2.cjs file
const listInfo = require('../callback2.cjs');

// Specify the ID to search for
let id = 'mcu453ed';

// Call the listInfo function with the specified ID and a callback function
listInfo(id, (err, msg) => {
    // Check if there is an error
    if (err) {
        // Log the error to the console
        console.error(err);
    } else {
        // Log the retrieved list information to the console
        console.log(msg);
    }
});
