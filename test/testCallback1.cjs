// Import the boardInfo function from the callback1.cjs file
const boardInfo = require('../callback1.cjs');

// Specify the ID to search for
let id = 'mcu453ed';

// Call the boardInfo function with the specified ID and a callback function
boardInfo(id, (error, msg) => {
    // Check if there is an error
    if (error) {
        // Log the error to the console
        console.error(error);
    } else {
        // Log the retrieved board information to the console
        console.log(msg);
    }
});
