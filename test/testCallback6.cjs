// Import the searchByName function from the callback6.cjs file
const searchByName = require('../callback6.cjs');

// Specify the name to search for
let name = "Thanos";

// Call the searchByName function with the specified name
searchByName(name);
