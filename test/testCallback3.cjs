// Import the cardInfo function from the callback3.cjs file
const cardInfo = require('../callback3.cjs');

// Specify the ID to search for
let id = 'qwsa221';

// Call the cardInfo function with the specified ID and a callback function
cardInfo(id, (err, msg) => {
    // Check if there is an error
    if (err) {
        // Log the error to the console
        console.error(err);
    } else {
        // Log the retrieved list information to the console
        console.log(msg);
    }
});
