const fs = require("fs");

// Function to retrieve board information by ID with a delay
const boardInfo = function (id, callback) {
  // Simulate a delay using setTimeout
  setTimeout(() => {
    // Read the contents of the boards_1.json file
    fs.readFile("./boards_1.json", "utf-8", (err, data) => {
      if (err) {
        // If there's an error reading the file, invoke the callback with the error
        callback(err, null);
        return;
      } else {
        // Parse the JSON data into an array of boards
        let boards = JSON.parse(data);

        // Filter the boards to find the one with the matching ID
        let info = boards.filter((board) => {
          // Return true if the board's ID matches the specified ID
          return board.id === id;
        });

        // Check if any matching boards were found
        if (info.length) {
          // Invoke the callback with the found board information
          callback(null, info);
        } else {
          // Invoke the callback with an error if no matching board was found
          callback("Board not found", null);
        }
      }
    });
  }, 2000); // Simulated delay of 2000 milliseconds (2 seconds)
};

// Export the boardInfo function for external use
module.exports = boardInfo;
