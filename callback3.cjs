// Import the 'fs' module for file system operations
const fs = require('fs');

// Define a function to retrieve list information by ID with a delay
const cardInfo = function (id, callback) {
    // Simulate a delay using setTimeout (3000 milliseconds or 3 seconds)
    setTimeout(() => {
        // Read the contents of the cards_1.json file
        fs.readFile('./cards_1.json', 'utf-8', (err, data) => {
            // Check if there's an error reading the file
            if (err) {
                // Invoke the callback with the error and null data
                callback(err, null);
                return;
            }
            else {
                // Parse the JSON data into an object representing lists
                let cards = JSON.parse(data);

                // Check if the specified ID exists in the lists
                if (cards[id]) {
                    // Invoke the callback with the information for the specified ID
                    callback(null, cards[id]);
                }
                else {
                    // Invoke the callback with an error if the ID is not found
                    callback('No data found', null);
                }
            }
        });
    }, 3000); // Simulated delay of 3000 milliseconds (3 seconds)
}

// Export the cardInfo function 
module.exports = cardInfo;
