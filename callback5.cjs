// Import the 'fs' module for file system operations
const fs = require('fs');
// Import the cardInfo function from the callback3.cjs file
const cardInfo = require('./callback3.cjs');
// Import the listInfo function from the callback2.cjs file
const listInfo = require('./callback2.cjs');

// Import the 'boardInfo' function from the callback1.cjs module
const boardInfo = require('./callback1.cjs');

// Define a function to retrieve board information by name with a delay
const searchByName = function(name) {
    // Simulate a delay using setTimeout (2500 milliseconds or 2.5 seconds)
    setTimeout(() => {
        // Read the contents of the boards_1.json file
        fs.readFile("./boards_1.json", "utf-8", (err, data) => {
            // Check if there's an error reading the file
            if (err) {
                // Log the error to the console
                console.error(err);
            } else {
                // Parse the JSON data into an array of boards
                let boards = JSON.parse(data);

                // Filter the boards to find the one with the matching name
                let id = boards.filter((board) => {
                    return board.name === name;
                }).map((board) => board.id);

                // Check if any matching boards were found
                if (id.length) {
                    // Call the boardInfo function with the found board's ID
                    boardInfo(id[0], (error, msg) => {
                        // Check if there is an error
                        if (error) {
                            // Log the error to the console
                            console.error(error);
                        } else {
                            // Log the retrieved board information to the console
                            console.log(msg);
                        }
                    });

                    // Call the listInfo function with the specified ID and a callback function
                    listInfo(id[0], (err, msg) => {
                    // Check if there is an error
                    if (err) {
                        // Log the error to the console
                        console.error(err);
                    } else {
                        // Log the retrieved list information to the console
                        console.log(msg);

                        //Filter out the id's of from list where name is 'Mind' and 'Space'
                        let cardId = msg.filter((card) => {
                            if(card.name === 'Mind' || card.name === 'Space'){
                                return true;
                            }
                        }).map((card) => card.id);

                        for(let index = 0; index < cardId.length; index++){

                            // Call the cardInfo function with the specified ID and a callback function
                            cardInfo(cardId[index], (err, msg) => {
                                // Check if there is an error
                                if (err) {
                                    // Log the error to the console
                                    console.error(err);
                                } else {
                                    // Log the retrieved list information to the console
                                    console.log(msg);
                                }
                            });
                        }   

                    }

                });

                } else {
                    // Log an error message if the board with the specified name is not found
                    console.error(`${name} not found`);
                }
            }
        });
    }, 2500); // Simulated delay of 2500 milliseconds (2.5 seconds)
}

// Export the searchByName function 
module.exports = searchByName;
